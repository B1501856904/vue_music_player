// 1.导入vue-router包
import VueRouter from 'vue-router'

// 导入组件
// import Home from  './components/tabbars/home.vue'
import Index from  './pages/index.vue'
import Home from  './pages/home.vue'
// import Artists from  './pages/artists.vue'
// import Listcate from  './pages/listcate.vue'
// import Search from  './pages/search.vue'
import Ucenter from  './pages/ucenter.vue'
// import Hot from './pages/musiclist/hot_list.vue'
// import King from './pages/musiclist/king_list.vue'
// import News from './pages/musiclist/news_list.vue'
// import MoreList from  './pages/morelist.vue'
import Login from './pages/ucenter/login.vue'
import Personal from './pages/ucenter/personal.vue'
import Register from './pages/ucenter/register.vue'
import ArtistsDetails from "./pages/artistsDetails/artistsDetails"
// import LicateDetails from "@/pages/licateDetails/licateDetails"






// 3.创建路由对象
var router = new VueRouter({
    routes:[
        // {path:'/',redirect:''},
        // {path:'/Home',component:Home},
        {path:'/',component:Index,redirect:'home',
        children:[
            {
                path:'home',
                component:Home,
                redirect:"/home/hot",
                children:[
                    {path:"hot",
                    component:resolve=>require(['./pages/musiclist/hot_list.vue'],resolve),meta:{title:'主页/musiclist/hot'}
                    
                    },
                    {path:"king",
                    component:resolve=>require(['./pages/musiclist/king_list.vue'],resolve),meta:{title:'主页/musiclist/king'}
                    },
                    {path:"news",
                    component:resolve=>require(['./pages/musiclist/news_list.vue'],resolve),meta:{title:'主页/musiclist/new'}
                    },
                    
                ]
            },
            {
                path:'ucenter/loginA',
                component:resolve=>require(['./pages/ucenter/login.vue'],resolve),meta:{title:'主页/导航/listcate'}
            },
            {
                path:'artists',
                component:resolve=>require(['./pages/artists.vue'],resolve),meta:{title:'主页/导航/artists'}
            },
            {
                path:'musicplay',
                name:'MusicPlay',
                component:resolve=>require(['./pages/musicplay.vue'],resolve),meta:{title:'主页/导航/artists'}
            },
            {
                path:'licateDetails',
                name:'LicateDetails',
                component:resolve=>require(['./pages/licateDetails/licateDetails.vue'],resolve),meta:{title:'主页/导航/artists'}
            },
            {
                path:"artistsdetails",
                name:"ArtistsDetails",
                component:ArtistsDetails
              },
            {
                path:'listcate',
                component:resolve=>require(['./pages/listcate.vue'],resolve),meta:{title:'主页/导航/listcate'}
            },
            
            {
                path:'mv',
                component:resolve=>require(['./pages/mv.vue'],resolve),meta:{title:'主页/mv/listcate'}
            },
            {
                path:'search',
                component:resolve=>require(['./pages/search.vue'],resolve),meta:{title:'主页/导航/search'}
            },
            {
                path:'ucenter',
                component:Ucenter,
                redirect:"/ucenter/login",
                children:[
                    {path:'login',component:Login},
                    {path:'personal',component:Personal},
                    {path:'register',component:Register},
                ]
            },
            {path:"morelist",
                    name:"MoreList",
                    component:resolve=>require(['./pages/morelist.vue'],resolve),meta:{title:'主页/MoreList'}
                    },
        ]},
        
    ],
    
  
    // linkActiveClass:'mui-active'
})

// router.beforeEach((to,from,next) => {
//     //路由中设置的needLogin字段就在to当中
//     if(window.localStorage.length === 2){
//         console.log(window.localStorage.length);
//         if(to.path === '/'){
//             next({path:'/home'})
//         }else{
//             next();
//         }
//     }else{
//         //如果没有session，访问任何页面，都会进入到登录页
//         if(to.path === '/ucenter/loginA'){//如果是登录页便直接next() -->解决注销后的循环执行bug
//             // console.log(1);
//             next();
//         }else{//否则跳转到登录页面
//             next({path:'/'});
//             // console.log(2);
//         }
//     }
// })
// router.beforeEach((to,from,next) => {
//     window.document.title = to.meta.title;
//     next();
// })
router.afterEach((to,from,next) => {
    window.scrollTo(0,0);
})
// 把路由对象暴露出去
export default router