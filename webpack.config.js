const path = require('path')
// 这个配置文件，其实就是一个js文件,通过node中的模块操作，向外暴露一个配置对象

const VueLoaderPlugin = require('vue-loader/lib/plugin')
// 热更新的第二步
const webpack = require('webpack')

const htmlWebpackPulugin = require('html-webpack-plugin')

module.exports = {
    entry:path.join(__dirname,'./src/main.js'),//入口，表示要使用webpack去打包那个文件
    output:{
        path:path.join(__dirname,'./dist'),//指定 打包好的文件，输出到那个目录中
        filename:'bundle.js'//输出文件的名称
    },
    devServer:{//配置dev-server命令参数的第二种形式，相对来说这种方法会麻烦一点
        // --contentBase src --open --port 3000
        port:2222,//设置运行的端口号
        contentBase:'src',//指定托管的目录
        // open:true//自动打开浏览器
        hot:true//热更新的第一步
    },
    plugins:[//配置插件的节点
        new webpack.HotModuleReplacementPlugin(),//new一个热更新模块对象，这是第三步
        new htmlWebpackPulugin({//创建一个在内存中生成的HTML页面的插件
            template:path.join(__dirname,'./src/index.html'),//指定模板页面，根据这里的路径，在内存中生成页面
            filename:'index.html'//也定生成页面的名称
        }),

        new VueLoaderPlugin()
    ],
    module:{//这个节点，用于配置所有的第三方模块加载器
        rules:[//所有的第三方模块的匹配规则
            {test:/\.css$/,use:['style-loader','css-loader']},//配置处理.css文件的第三方loader
            {test:/\.scss$/,use:['style-loader','css-loader','sass-loader']},//配置处理.css文件的第三方loader
            {test:/\.(jpg|png|gif)$/,use:'url-loader?limit=200&name=[hash:8]-[name].[ext]'},//处理字体，图片路径的loader
            {test:/\.(ttf|eot|svg|woff|woff2)$/,use:'url-loader'},
            {test:/\.vue$/,use:'vue-loader'}//处理.vue文件的loader
        ]
    },
    resolve:{
        alias:{//修改Vue被导入时候的包的路径
            "vue$":"vue/dist/vue.js"
        }
    }
}