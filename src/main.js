import Vue from 'vue'
import VueRouter from 'vue-router'
import $ from 'jquery'
Vue.use(VueRouter)
import qs from 'qs'//处理POST要提交的数据
Vue.prototype.$qs = qs

import Axios from "axios"

Vue.prototype.$axios  = Axios;
Vue.prototype.HOST = "/baidu_music_api"



require('video.js/dist/video-js.css')
require('vue-video-player/src/custom-theme.css')
import VideoPlayer from 'vue-video-player'
Vue.use(VideoPlayer);

// Vue.prototype.HOSt = "/baidu_music_api"
// // 导入MintUi
import 'mint-ui/lib/style.css'
// // import '../lib/mint-ui/lib/style.css'

// // 导入mui的样式表
// import '../lib/mui/css/mui.min.css'
// import '../lib/mui/css/icons-extra.css'

// // 导入bs的样式表
// import '../lib/bs/css/bootstrap.min.css'

// // 按需导入，把组件组册为全局组件
import { Swipe, SwipeItem, Button, Header ,Navbar, TabItem } from 'mint-ui';

Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);
Vue.component(Button.name, Button);
Vue.component(Header.name, Header);
Vue.component(Navbar.name, Navbar);
Vue.component(TabItem.name, TabItem);
 
// 导入主体组件
import App from './components/App.vue'

// 导入自定义路由模块
import router from './router.js'

// new Vue({
//     el:'app',
//     data:{},
//     render:c=>c(App),
//     router
// })

new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
  })
  
